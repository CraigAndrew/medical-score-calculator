import { Injectable } from "@angular/core";
import { ScoreCriterion, ScoreResult } from "src/_models";

// Ideally this should be external config or fetched from a database
const SCORE_CRITERIA: ScoreCriterion[] = [
  new ScoreCriterion("CONFUSION", "Confusion"),
  new ScoreCriterion("BUN", "BUN > 19 mg/dL (> 7 mmol/L)"),
  new ScoreCriterion("RESPIRATORY_RATE", "Respiratory Rate ≥ 30"),
  new ScoreCriterion(
    "SYSTOLIC_BP",
    "Systolic BP < 90 mmHg or Diastolic BP ≤ 60 mmHg"
  ),
  new ScoreCriterion("AGE_65_AND_OVER", "Age ≥ 65")
];

// Ideally this should be external config or fetched from a database
// Decided to store this as a hash for optimal lookup speed for now
// but could convert it to a collection of ScoreResult models to improve maintainability
const SCORE_RESULTS_HASH = {
  0: new ScoreResult(
    "Low risk group: 0.6% 30-day mortality.",
    "Consider outpatient treatment."
  ),
  1: new ScoreResult(
    "Low risk group: 2.7% 30-day mortality.",
    "Consider outpatient treatment."
  ),
  2: new ScoreResult(
    "Moderate risk group: 6.8% 30-day mortality.",
    "Consider inpatient treatment or outpatient with close followup."
  ),
  3: new ScoreResult(
    "Severe risk group: 14.0% 30-day mortality.",
    "Consider inpatient treatment with possible intensive care admission."
  ),
  4: new ScoreResult(
    "Highest risk group: 27.8% 30-day mortality.",
    "Consider inpatient treatment with possible intensive care admission."
  ),
  5: new ScoreResult(
    "Highest risk group: 27.8% 30-day mortality.",
    "Consider inpatient treatment with possible intensive care admission."
  )
};

@Injectable()
export class ScoreCalculatorDataService {
  getScoreCalculatorConfigData() {
    return SCORE_CRITERIA;
  }

  getScoreResultsConfigData() {
    return SCORE_RESULTS_HASH;
  }
}
