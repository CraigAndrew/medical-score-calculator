export class ScoreResult {
  riskGroup: string;
  treatment: string;

  constructor(riskGroup, treatment) {
    this.riskGroup = riskGroup;
    this.treatment = treatment;
  }
}
