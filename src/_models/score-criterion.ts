export class ScoreCriterion {
  key: string;
  name: string;
  value: boolean;

  constructor(key, name, value = false) {
    this.key = key;
    this.name = name;
    this.value = value;
  }
}
