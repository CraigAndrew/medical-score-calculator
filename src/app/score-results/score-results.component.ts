import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "score-results",
  templateUrl: "./score-results.component.html",
  styleUrls: ["./score-results.component.scss"]
})
export class ScoreResultsComponent implements OnInit {
  @Input() points: string;
  @Input() riskGroup: string;
  @Input() treatment: string;

  constructor() {}

  ngOnInit() {}
}
