import { Component, OnInit } from "@angular/core";
import { ScoreCalculatorDataService } from "../../_services";
import { MatTableDataSource } from "@angular/material";

@Component({
  selector: "score-calculator",
  templateUrl: "./score-calculator.component.html",
  styleUrls: ["./score-calculator.component.scss"]
})
export class ScoreCalculatorComponent implements OnInit {
  displayedColumns: string[] = ["criterion"];
  dataSource: MatTableDataSource<any>;
  score = 0;
  pointsRiskGroupTreatmentHash = {};

  constructor(public scoreCalculatorDataService: ScoreCalculatorDataService) {}

  ngOnInit() {
    this.dataSource = new MatTableDataSource(
      this.scoreCalculatorDataService.getScoreCalculatorConfigData()
    );
    this.pointsRiskGroupTreatmentHash = this.scoreCalculatorDataService.getScoreResultsConfigData();
  }

  setValue(key, e) {
    this.dataSource.data.find(item => item.key === key).value = e.checked;
    this.score = this.dataSource.data.reduce((acc, curr) => {
      if (curr.value) {
        acc++;
      }
      return acc;
    }, 0);
  }
}
