import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { ThemingService } from "./theming.service";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [ToolbarComponent],
  exports: [ToolbarComponent],
  providers: [ThemingService]
})
export class CoreModule {}
