import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatTableModule } from "@angular/material/table";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { ScoreResultsComponent } from "./score-results/score-results.component";
import { ScoreCalculatorComponent } from "./score-calculator/score-calculator.component";
import { ScoreCalculatorDataService } from "../_services";

@NgModule({
  declarations: [AppComponent, ScoreCalculatorComponent, ScoreResultsComponent],
  imports: [
    CoreModule,
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    MatTableModule
  ],
  providers: [ScoreCalculatorDataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
